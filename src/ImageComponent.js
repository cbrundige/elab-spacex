import React, { Component } from "react";
import "./index.css";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Dropdown from "react-bootstrap/Dropdown";
import ListGroupItem from "react-bootstrap/ListGroupItem";
import ListGroup from "react-bootstrap/ListGroup";
import Jumbotron from "react-bootstrap/Jumbotron";
import Container from "react-bootstrap/Jumbotron";

class ImageComponent extends Component {
  render() {
    const { rocketData, launchData } = this.props;
    const imgUrl = rocketData.flickr_images[0];
    const styles = {
      backgroundImage: `url(${imgUrl})`,
      backgroundRepeat: "no-repeat",
      backgroundSize:"cover",
    };
    const pStyle = {
      backgroundColor: "rgba(10,10,10,0.4)",
      padding: "10px",
      color: "white",
      margin: "25px"
    };

    //iterate through an array -> return all data that matches to rocket name
    let takeOff = [];
    for (let launch in launchData) {
      if (launchData[launch].rocket.rocket_name === rocketData.rocket_name) {
        takeOff.push(launchData[launch].mission_name);
      }
    }
    let tMinus = takeOff.map((launch, index) => {
      return <ListGroup.Item key={index}>{launch}</ListGroup.Item>;
    });
    return (
      <Jumbotron fluid style={styles}>
        <Container style={pStyle}>
          <h2>{rocketData.rocket_name}</h2>
          <p>{rocketData.description}</p>
          <Dropdown>
            <Dropdown.Toggle variant="primary" id="dropdown-basic">
              Mission Data
            </Dropdown.Toggle>
            <Dropdown.Menu>{tMinus}</Dropdown.Menu>
          </Dropdown>
        </Container>
      </Jumbotron>
    );
  }
}

export default ImageComponent;
