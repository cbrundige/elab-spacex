import React, { Component } from "react";

class RocketSpec extends Component {
  render() {
    const { stageData } = this.props;
    return (
      <div className="wrapper">
        <div className="outer-wrap">
          <h1>STAGE ONE</h1>

          <div className="data-grid-wrapper">
            <div className="spec-item">
              <div className="innerbox">
                <p>BURN TIME</p>
              </div>
              <h3>{stageData.burn_time_sec}sec</h3>
            </div>

            <div className="spec-item">
              <div className="innerbox">
                <p>No. OF ENGINES</p>
              </div>
              <h3>{stageData.engines}</h3>
            </div>

            <div className="spec-item">
              <div className="innerbox">
                <p>FUEL</p>
              </div>
              <h3>{stageData.fuel_amount_tons}tons</h3>
            </div>

            <div className="spec-item">
              <div className="innerbox">
                <p> REUSABLE</p>
              </div>
              <h3>{stageData.reusable.toString()}</h3>
            </div>

            <div className="spec-item">
              <div className="innerbox">
                <p>SEA LEVEL THRUST</p>
              </div>
              <h3>{stageData.thrust_sea_level.kN}kN</h3>
            </div>

            <div className="spec-item">
              <div className="innerbox">
                <p>VACUUM THRUST</p>
              </div>
              <h3>{stageData.thrust_vacuum.kN}kN</h3>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default RocketSpec;
