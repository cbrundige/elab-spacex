import React, { Component } from "react";

class RocketSpec extends Component {
  render() {
    const { rocketSpec } = this.props;
    return (
      <div className="wrapper">
        <div className="outer-wrap">
          <h1>ROCKET DATA</h1>

          <div className="data-grid-wrapper">
            <div className="spec-item">
              <div className="innerbox">
                <p>BOOSTERS</p>
              </div>
              <h3>{rocketSpec.boosters}</h3>
            </div>

            <div className="spec-item">
              <div className="innerbox">
                <p>DESIGNED BY</p>
              </div>
              <h3>{rocketSpec.company}</h3>
            </div>

            <div className="spec-item">
              <div className="innerbox">
                <p>Cost</p>
              </div>
              <h3>{rocketSpec.cost_per_launch}USD</h3>
            </div>

            <div className="spec-item">
              <div className="innerbox">
                <p> FIRST FLIGHT IN</p>
              </div>
              <h3>{rocketSpec.country}</h3>
            </div>

            <div className="spec-item">
              <div className="innerbox">
                <p>DIAMETER</p>
              </div>
              <h3>{rocketSpec.diameter.meters}m</h3>
            </div>

            <div className="spec-item">
              <div className="innerbox">
                <p>HEIGHT</p>
              </div>
              <h3>{rocketSpec.height.meters}m</h3>
            </div>

            <div className="spec-item">
              <div className="innerbox">
                <p>MASS</p>
              </div>
              <h3> {rocketSpec.mass.kg}kg</h3>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default RocketSpec;
