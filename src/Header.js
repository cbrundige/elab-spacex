import React, { Component } from "react";
import Navbar from "react-bootstrap/Navbar";



class Header extends Component {
  
  render() {
    const { sortActive } = this.props;
    return (
      <Navbar expand="lg" variant="dark" bg="dark">
        <Navbar.Brand>
          <h1>SpaceX</h1>
        </Navbar.Brand>
        <button variant="outline" onClick={sortActive}>
          Sort by active
        </button>
      </Navbar>
    );
  }
}

export default Header;
