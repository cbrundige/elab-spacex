import React, { Component } from "react";
import RocketDataTable from "./RocketDataTable";
import ImageComponent from "./ImageComponent";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

class RocketDataWrap extends Component {
  render() {
    const { rocketData, launchData } = this.props;
    return (
      <div className="RocketDataWrap">
        <Row >
        <Col>
         <ImageComponent rocketData={rocketData} launchData={launchData} />
        </Col>
        <Col>
          <RocketDataTable rocketData={rocketData} />
          </Col>
        </Row>
      </div>
    );
  }
}

export default RocketDataWrap;
