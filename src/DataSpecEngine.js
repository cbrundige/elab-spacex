import React, { Component } from "react";

class DataSpecEngine extends Component {
  render() {
    const { engineData } = this.props;
    return (
      <div className="wrapper">
        <div className="outer-wrap">
          <h1>ENGINE SPECS</h1>
          <h3>
            {engineData.type} {engineData.version}
          </h3>

          <div className="data-grid-wrapper">
            <div className="spec-item">
              <div className="innerbox">
                <p>LAYOUT</p>
              </div>
              <h3>{engineData.layout}</h3>
            </div>

            <div className="spec-item">
              <div className="innerbox">
                <p>ENGINE LOSS MAX</p>
              </div>
              <h3>{engineData.engine_loss_max}</h3>
            </div>

            <div className="spec-item">
              <div className="innerbox">
                <p> ENGINE NUMBER</p>
              </div>
              <h3>{engineData.number}</h3>
            </div>

            <div className="spec-item">
              <div className="innerbox">
                <p>PROPELLANT 1</p>
              </div>
              <h3>{engineData.propellant_1}</h3>
            </div>

            <div className="spec-item">
              <div className="innerbox">
                <p>PROPELLANT 2</p>
              </div>
              <h3>{engineData.propellant_2}</h3>
            </div>
            <div className="spec-item">
              <div className="innerbox">
                <p>THRUST TO WEIGHT</p>
              </div>
              <h3> {engineData.thrust_to_weight}</h3>
            </div>
            <div className="spec-item">
              <div className="innerbox">
                <p>THRUST VACUUM</p>
              </div>
              <h3> {engineData.thrust_vacuum.kN}kN</h3>
            </div>
            <div className="spec-item">
              <div className="innerbox">
                <p>Sea Level thrust</p>
              </div>
              <h3> {engineData.thrust_sea_level.kN}kN</h3>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default DataSpecEngine;

/*


number,
propellant_1,
propellant_2,
thrust_to_weight,
*/
