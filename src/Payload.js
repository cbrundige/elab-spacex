import React, { Component } from "react";

class Payload extends Component {
  render() {
    const { payload } = this.props;

    return (
      <div className="wrapper">
        <div className="outer-wrap">
          <h1>PAYLOAD DATA</h1>
          <div className="data-grid-wrapper">
            <div className="spec-item">
              <div className="innerbox">
                <p>name</p>
              </div>
              <h3>{payload[0].name}</h3>
            </div>

            <div className="spec-item">
              <div className="innerbox">
                <p>ID</p>
              </div>
              <h3>{payload[0].id}</h3>
            </div>

            <div className="spec-item">
              <div className="innerbox">
                <p>Mass</p>
              </div>
              <h3>{payload[0].kg}kg</h3>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Payload;
