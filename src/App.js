import React, { Component } from "react";
import './index.css';
import RocketDataWrap from "./RocketDataWrap";
import Header from "./Header.js";
import Footer from "./Footer.js";
class App extends Component {
  //state
  state = {
    rockets: [],
    launches: []
  };

  //lifecycle hooks
  //1. on mount get data from APIs and push to state
  componentDidMount() {
    const urlRockets = "https://api.spacexdata.com/v3/rockets";
    const urlLaunches = "https://api.spacexdata.com/v3/launches";
    fetch(urlRockets)
      .then(resp => resp.json())
      .then(data => this.setState({ rockets: data }));
    fetch(urlLaunches)
      .then(resp => resp.json())
      .then(data => this.setState({ launches: data }));
  }

  //1. sort by active

  sortActive = () => {
    //1.copy state
    const { rockets } = this.state;

    this.setState({
      rockets: rockets.filter((rocket, index) => {
        return rocket.active !== false;
      })
    });

    console.log(this.state);
  };

  render() {
    const { rockets, launches } = this.state;
    const rocketShips = rockets.map((rocket, index) => {
      return (
        <RocketDataWrap rocketData={rockets[index]} launchData={launches} />
      );
    });

    return (
      <div className="App">
        <Header className="padding-bottom" sortActive={this.sortActive} />
        {rocketShips}
        <Footer />
      </div>
    );
  }
}

export default App;
