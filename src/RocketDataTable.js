import React, { Component } from "react";
import RocketDataWrap from "./RocketDataWrap";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import DataSpecEngine from "./DataSpecEngine.js";
import RocketSpec from "./RocketSpec.js";
import StageOne from "./StageOne.js";
import StageTwo from "./StageTwo.js";
import Payload from "./Payload.js";

class RocketDataTable extends Component {
  render() {
    const { rocketData } = this.props;
    const {
      engines,
      first_stage: stageOne,
      second_stage: stageTwo,
      payload_weights: payload
    } = rocketData;

    return (
      <div className="wrapper">
        <div className="data-body">
          <RocketSpec rocketSpec={rocketData} />
          <DataSpecEngine engineData={engines} />
          <StageOne stageData={stageOne} />
          <StageTwo stageTwoData={stageTwo} />
          <Payload payload={payload} />
        </div>
      </div>
    );
  }
}
export default RocketDataTable;
