import React, { Component } from "react";

class StageTwo extends Component {
  render() {
    const { stageTwoData: data } = this.props;
    return (
      <div className="wrapper">
        <div className="outer-wrap">
          <h1>ROCKET DATA</h1>

          <div className="data-grid-wrapper">
            <div className="spec-item">
              <div className="innerbox">
                <p>No. OF ENGINES</p>
              </div>
              <h3>{data.engines}</h3>
            </div>

            <div className="spec-item">
              <div className="innerbox">
                <p>BURN TIME</p>
              </div>
              <h3>{data.burn_time_sec}sec</h3>
            </div>

            <div className="spec-item">
              <div className="innerbox">
                <p>FUEL</p>
              </div>
              <h3>{data.fuel_amount_tons}tons</h3>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default StageTwo;
